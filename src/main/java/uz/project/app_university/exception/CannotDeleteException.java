package uz.project.app_university.exception;
// Bahodir Hasanov 7/6/2022 6:41 PM

public class CannotDeleteException extends RuntimeException {
    public CannotDeleteException(String message) {
        super(message);
    }
}
