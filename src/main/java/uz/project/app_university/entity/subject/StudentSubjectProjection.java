package uz.project.app_university.entity.subject;
// Bahodir Hasanov 7/8/2022 12:34 PM

public interface StudentSubjectProjection {
     Integer getId();
     String getName();
     Integer getMark();
}
