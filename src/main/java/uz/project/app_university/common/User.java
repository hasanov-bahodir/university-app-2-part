package uz.project.app_university.common;

import lombok.Data;
import lombok.NoArgsConstructor;
import uz.project.app_university.enums.Role;


import javax.persistence.*;

// Bahodir Hasanov 7/6/2022 10:47 AM
@MappedSuperclass
@NoArgsConstructor
@Data
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false,unique = true)
    private String username;

    @Column(nullable = false)
    private String password;

    @Enumerated(EnumType.STRING)
    private Role role;

    public User(String name, String username, String password, Role role) {
        this.name = name;
        this.username = username;
        this.password = password;
        this.role = role;
    }

}
