package uz.project.app_university.enums;
// Bahodir Hasanov 7/6/2022 11:05 AM

public enum Role {
    ROLE_TEACHER,
    ROLE_STUDENT,
    ROLE_ADMIN,
    ROLE_SUPER_ADMIN
}
